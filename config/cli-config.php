<?php

use Core\Container\Container;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

/**
 * @var Container $container
 */
$container = require "container.php";
return ConsoleRunner::createHelperSet($container->get(EntityManager::class));