<?php

use Core\Container\Container;

$coreDependencies = require __DIR__ . '/dependencies/core.php';
$controllersDependencies = require __DIR__ . '/dependencies/controllers.php';
$servicesDependencies = require __DIR__ . '/dependencies/services.php';
$middlewaresDependencies = require __DIR__ . '/dependencies/middlewares.php';

$protectedRoutes = require __DIR__ . '/app/routes/protected.php';
$hiddenRoutes = require __DIR__ . '/app/routes/hidden.php';

$dbConfig = require __DIR__ . '/db.php';

$dependencies = array_merge(
    $coreDependencies,
    $controllersDependencies,
    $servicesDependencies,
    $middlewaresDependencies
);

$container = new Container($dependencies);
$container->set("DB", $dbConfig);
$container->set("HIDDEN-ROUTES", $hiddenRoutes);
$container->set("PROTECTED-ROUTES", $protectedRoutes);

return $container;