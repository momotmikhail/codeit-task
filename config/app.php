<?php

use Core\App;
use Core\Container\Container;

/**
 * @var Container $container
 */
$container = require __DIR__ . '/container.php';
$app = $container->get(App::class);
require __DIR__ . '/app/routes.php';
require __DIR__ . '/app/middlewares.php';

return $app;

