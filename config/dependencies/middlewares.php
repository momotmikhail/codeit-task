<?php

use App\Services\AuthService;
use App\Services\PageService;
use Core\Interfaces\IView;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use App\Controllers;
use App\Middlewares;

return [

    Middlewares\AuthMiddleware::class => function (ContainerInterface $container){
        return new App\Middlewares\AuthMiddleware(
            $container->get(AuthService::class)
        );
    },

    Middlewares\ValidateCreatePageRequest::class => function (ContainerInterface $container){
        return new Middlewares\ValidateCreatePageRequest(
            $container->get(PageService::class),
            $container->get(IView::class)
        );
    }

];