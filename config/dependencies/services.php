<?php

use App\Services;
use Core\Http\Cookie;
use Core\Http\Session;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

return [

    Services\AuthService::class => function(ContainerInterface $container){
        return new Services\AuthService(
            $container->get(EntityManager::class),
            new Session(new Cookie()),
            $container->get("HIDDEN-ROUTES"),
            $container->get("PROTECTED-ROUTES")
        );
    },

    Services\PageService::class => function(ContainerInterface $container){
        return new Services\PageService($container->get(EntityManager::class));
    }
];