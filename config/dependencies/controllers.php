<?php

use App\Services\AuthService;
use App\Services\PageService;
use Core\Interfaces\IView;
use Psr\Container\ContainerInterface;
use App\Controllers;
use App\Middlewares;

return [

    Controllers\HomeAction::class => function(ContainerInterface $container){
        return new Controllers\HomeAction(
            $container->get(IView::class),
            $container->get(PageService::class)
        );
    },
    Controllers\PageAction::class => function(ContainerInterface $container){
        return new Controllers\PageAction(
            $container->get(PageService::class),
            $container->get(IView::class)
        );
    },

    Controllers\Auth\LoginShowAction::class => function(ContainerInterface $container){
        return new Controllers\Auth\LoginShowAction($container->get(IView::class));
    },
    Controllers\Auth\LoginAction::class => function(ContainerInterface $container){
        return new Controllers\Auth\LoginAction(
            $container->get(AuthService::class),
            $container->get(IView::class)
        );
    },
    Controllers\Auth\LogoutAction::class => function(ContainerInterface $container){
        return new Controllers\Auth\LogoutAction($container->get(AuthService::class));
    },

    Controllers\Admin\CabinetAction::class => function(ContainerInterface $container){
        return new Controllers\Admin\CabinetAction($container->get(IView::class));
    },
    Controllers\Admin\Page\CreateFormAction::class => function(ContainerInterface $container){
        return new Controllers\Admin\Page\CreateFormAction(
            $container->get(PageService::class),
            $container->get(IView::class)
        );
    },

    Controllers\Admin\Page\CreateAction::class => function(ContainerInterface $container){
        return new Controllers\Admin\Page\CreateAction(
            $container->get(PageService::class),
            $container->get(IView::class)
        );
    },
    Controllers\Admin\Page\IndexAction::class => function(ContainerInterface $container){
        return new Controllers\Admin\Page\IndexAction(
            $container->get(PageService::class),
            $container->get(IView::class)
        );
    },
    Controllers\Admin\Page\DeleteAction::class => function(ContainerInterface $container){
        return new Controllers\Admin\Page\DeleteAction(
            $container->get(PageService::class)
        );
    },

    Controllers\Errors\PageNotFound::class => function(ContainerInterface $container){
        return new Controllers\Errors\PageNotFound(
            $container->get(IView::class)
        );
    },
];