<?php

use App\Controllers\Errors\PageNotFound;
use Core\App;
use Core\Interfaces\IResolver;
use Core\Interfaces\Router\IRouter;
use Core\Interfaces\IView;
use Core\Resolver\ActionResolver;
use Core\Router\RouteCollection;
use Core\Router\Router;
use Core\View\TwigView;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Psr\Container\ContainerInterface;

return [

    App::class => function (ContainerInterface $container) {
        return new App(
            $container->get(Router::class),
            $container->get(IResolver::class),
            new PageNotFound($container->get(IView::class))
        );
    },
    IRouter::class => function(ContainerInterface $container){
        return new Router(new RouteCollection());
    },
    IResolver::class => function(ContainerInterface $container){
        return new ActionResolver($container);
    },
    IView::class => function(ContainerInterface $container){
        return new TwigView($container->get(Twig\Environment::class), '.html.twig');
    },
    Twig\Environment::class => function(ContainerInterface $container)
    {
        $templateDir = 'views';
        $debug = false;

        $loader = new Twig\Loader\FilesystemLoader();
        $loader->addPath($templateDir);

        $environment = new Twig\Environment($loader, [
            'cache' => false,
            'debug' => $debug,
            'strict_variables' => $debug,
            'auto_reload' => $debug,
        ]);

        if ($debug) {
            $environment->addExtension(new Twig\Extension\DebugExtension());
        }

        return $environment;
    },
    EntityManager::class => function (ContainerInterface $container) {

        $config = Setup::createAnnotationMetadataConfiguration(
            array("src/App/Entities"),
            true, null, null, false);
        $connectionParams = $container->get("DB");
        return EntityManager::create($connectionParams, $config);
    },
];