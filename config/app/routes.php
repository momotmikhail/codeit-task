<?php
use App\Controllers;
use App\Middlewares;
use Core\App;

/**
 * @var App $app
 */

//Home page
$app->get("/", Controllers\HomeAction::class);

$app->get("/login", Controllers\Auth\LoginShowAction::class);
$app->post("/login", Controllers\Auth\LoginAction::class);
$app->post("/logout", Controllers\Auth\LogoutAction::class);

$app->get("/admin", Controllers\Admin\CabinetAction::class);
$app->get("/admin/page/index", Controllers\Admin\Page\IndexAction::class);// view
$app->get("/admin/page/create", Controllers\Admin\Page\CreateFormAction::class);// view
$app->post("/admin/page/create", [
    Middlewares\ValidateCreatePageRequest::class,
    Controllers\Admin\Page\CreateAction::class
]);
$app->post("/admin/page/delete/{id:\d+}", Controllers\Admin\Page\DeleteAction::class);

//$app->get("/pages", Controllers\Page\PageIndexAction::class);
$app->get("/{page:[a-z0-9-]+$}", Controllers\PageAction::class);