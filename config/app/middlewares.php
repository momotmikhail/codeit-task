<?php

use App\Middlewares;
use Core\App;

/**
 * @var App $app
 */
$app->middleware(Middlewares\AuthMiddleware::class);