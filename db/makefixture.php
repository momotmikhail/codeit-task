<?php

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Fixture\AdminFixture;
use Fixture\AppFixture;

ini_set('display_errors', 1);
error_reporting(E_ALL);
chdir(dirname(__DIR__));
require 'vendor/autoload.php';


$container = require "config/container.php";
$em = $container->get(EntityManager::class);

$loader = new Loader();
$loader->addFixture(new AppFixture());

$purger = new ORMPurger();
$executor = new ORMExecutor($em, $purger);
$executor->execute($loader->getFixtures());
