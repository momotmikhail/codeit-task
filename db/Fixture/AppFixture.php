<?php


namespace Fixture;


use App\Entities\Page;
use App\Entities\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixture extends AbstractFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setLogin("admin");
        $user->setPassword("admin");

        for ($i= 0; $i < 10; $i++){
            $page = new Page();
            $page->setTitle("Page title " . $i);
            $page->setDescription("Page desc  " . $i);
            $page->setUrl("/page" . $i);
            $page->setContent($i ." Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." . $i);
            $page->setUser($user);
            $manager->persist($page);
        }
        $manager->persist($user);
        $manager->flush();
    }
}