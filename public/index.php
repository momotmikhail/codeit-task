<?php

use Core\Http\Session;
use Zend\Diactoros\ServerRequestFactory;
use Zend\HttpHandlerRunner\Emitter\SapiEmitter;

ini_set('display_errors', 1);
error_reporting(E_ALL);
chdir(dirname(__DIR__));
require 'vendor/autoload.php';

$request = ServerRequestFactory::fromGlobals();

$app = require "config/app.php";
$response = $app->run($request);

$emitter = new SapiEmitter();
$emitter->emit($response);

//$cookie = new \Core\Http\Cookie();
//$cookie->remove(Session::COOKIE_PHPSESSID_KEY);
//unset($_COOKIE[Session::COOKIE_PHPSESSID_KEY]);

