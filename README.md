Deploying of project(Application needs common enter point - public/index.php)

Default deploying:

    edit config/db.php - for DB connection
    composer install - install dependencies
    composer build - create DB tables and make fixture by default values
    composer serve - run PHP dev server
    
    App is available on - http://localhost:9000/

NGINX deploying:

    #Config for NGINX (/etc/nginx/nginx.conf):

    user www-data;
    worker_processes auto;
    pid /run/nginx.pid;
    include /etc/nginx/modules-enabled/*.conf;
    
    events {
        worker_connections 768;
        # multi_accept on;
    }
    
    http {
        
        server { 
        listen 80; 
        server_name test-task.loc; 
    
        # Путь к папке с кодом
        root /var/www;
    
        index index.php index.htm index.html;
    
            location / {
            try_files $uri $uri/ /index.php;
            }
    
            location ~ \.php$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/run/php/php7.3-fpm.sock;
            }
        }
    }

 
