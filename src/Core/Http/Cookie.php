<?php

namespace Core\Http;

use Core\Interfaces\Http\ICookie as CookieInterface;

class Cookie implements CookieInterface
{
    public function set(string $name, $value = null, int $time = null, string $path = "/",
                               string $domain = "", bool $secure = false, bool $httponly = false) : void
    {
        if ($name !== '' || \PHP_VERSION_ID < 70000) {
            if (!\preg_match('/[=,; \\t\\r\\n\\013\\014]/', $name)) {
                if ($time == null)
                    $time = time() + 3600;
                $value = serialize($value);
                setcookie($name, $value, $time, $path, $domain, $secure, $httponly);
            }
        }
        else{
            exit("Invalid cookie name - cookie can not have empty name [Starting from PHP 7");
        }
    }

    public function get(string $name)
    {
        if (isset($_COOKIE[$name]))
            return unserialize($_COOKIE[$name]);
        else
            return null;
    }

    public function exists(string $name) : bool
    {
        if(isset($_COOKIE[$name]))
            return true;
        return false;
    }

    public function remove(string $name, string $path = "/", $domain = null) : void
    {
        if ($domain == null){
            $domain =  $_SERVER['SERVER_NAME'];
        }
        setcookie($name, "/", time() - 3600);
        unset($_COOKIE[$name]);
    }
}