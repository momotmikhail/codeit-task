<?php

namespace Core\Http;

use Core\Interfaces\Http\ISession as SessionInterface;

class Session implements SessionInterface
{
    private $cookie;
    public const COOKIE_PHPSESSID_KEY = 'PHPSESSID';

    public function __construct(Cookie $cookie)
    {
        $this->cookie = $cookie;
    }

    public function start() : void
    {
        if (!$this->sessionExists())
            session_start();
    }

    public function set(string $key, $value) : void
    {
        $this->start();
        $_SESSION[$key] = serialize($value);
    }

    public function get(string $key)
    {
        if($this->sessionExists() && isset($_SESSION[$key]))
            return unserialize($_SESSION[$key]);
        else{
            $this->cookie->remove(Session::COOKIE_PHPSESSID_KEY);
            return null;
        }
    }

    public function getName() : string
    {
        if($this->sessionExists())
            return session_name();
        else
            throw new Exception('ISession does not exist');
    }

    public function setName(string $name) : void
    {
        if(!$this->sessionExists())
            session_name($name);
    }

    public function sessionExists() : bool
    {
        if (session_status() == PHP_SESSION_ACTIVE)
            return true;
        else
            return false;
    }

    public function cookieExists() : bool
    {
        if($this->cookie->exists(Session::COOKIE_PHPSESSID_KEY))
            return true;
        return false;
    }

    public function destroy() : void
    {
        if ($this->cookieExists() && $this->sessionExists())
        {
            session_destroy();
            $this->cookie->remove(Session::COOKIE_PHPSESSID_KEY);
            unset($_COOKIE[Session::COOKIE_PHPSESSID_KEY]);
        }
    }
}