<?php


namespace Core\View;


use Core\Interfaces\IView;
use Twig\Environment;

class TwigView implements IView
{
    private $twig;
    private $extension;

    public function __construct(Environment $twig, $extension)
    {
        $this->twig = $twig;
        $this->extension = $extension;
    }

    public function render($name, array $params = []): string
    {
        return $this->twig->render($name . $this->extension, $params);
    }
}