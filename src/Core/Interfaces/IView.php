<?php

namespace Core\Interfaces;


interface IView
{
    public function render($name, array $params = []): string;
}