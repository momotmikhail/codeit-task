<?php

namespace Core\Interfaces;

interface IResolver
{
    public function resolve($handler): callable;
}