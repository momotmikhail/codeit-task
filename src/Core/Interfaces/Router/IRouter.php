<?php

namespace Core\Interfaces\Router;

use Core\Router\Result;
use Psr\Http\Message\ServerRequestInterface;

interface IRouter
{
    public function match(ServerRequestInterface $request): Result;
    public function addRoute(IRoute $route);
}