<?php

namespace Core\Interfaces\Router;

interface IRoute
{
    public function getRoute(): string ;
    public function getHandler();
    public function getMethod(): string;
}