<?php

namespace Core\Interfaces\Http;

interface ICookie
{
    public function set(string $name, $value = null, int $time = null) : void;
    public function get(string $name);
    public function exists(string $name) : bool;
    public function remove(string $name, string $path = "/", $domain = null) : void;
}