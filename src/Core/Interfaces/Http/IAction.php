<?php

namespace Core\Interfaces\Http;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface IAction
{
    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface;
}