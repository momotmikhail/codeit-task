<?php

namespace Core\Interfaces\Http;

interface ISession
{
    public function start() : void;

    public function set(string $key, $value) : void;
    public function get(string $key);

    public function getName() : string;
    public function setName(string $name) : void;

    public function sessionExists() : bool;
    public function cookieExists() : bool;

    public function destroy() : void;
}