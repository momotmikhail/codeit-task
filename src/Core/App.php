<?php

namespace Core;

use Core\Interfaces\IResolver;
use Core\Interfaces\Router\IRouter;
use Core\Pipeline\Pipeline;
use Core\Router\Exceptions\RequestNotMatchedException;
use Core\Router\IRoute;
use Core\Router\Route;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class App extends Pipeline
{
    private $router;
    private $resolver;
    private $default;

    public function __construct(IRouter $router, IResolver $resolver, callable $default)
    {
        $this->router = $router;
        $this->resolver = $resolver;
        $this->default = $default;
    }

    public function middleware($middleware)
    {
        $this->pipe($this->resolver->resolve($middleware));
    }

    public function run(ServerRequestInterface $request): ResponseInterface
    {
        $defaultAction = $this->default;
        try {

            $result = $this->router->match($request);
            $action = $this->resolver->resolve($result->getHandler());

            foreach ($result->getAttributes() as $attribute => $value)
                $request = $request->withAttribute($attribute, $value);

            $this->pipe($action);
            $response = $this($request, $defaultAction);

        }catch (RequestNotMatchedException $e){
            $response = $defaultAction($request);
        }
        return $response;
    }

    public function get(string $route, $handler) : void
    {
        $this->router->addRoute(new Route($route, $handler, "GET"));
    }

    public function post(string $route, $handler) : void
    {
        $this->router->addRoute(new Route($route, $handler, "POST"));
    }

    public function put(string $route, $handler) : void
    {
        $this->router->addRoute(new Route($route, $handler, "PUT"));
    }

    public function delete(string $route, $handler) : void
    {
        $this->router->addRoute(new Route($route, $handler, "DELETE"));
    }
}