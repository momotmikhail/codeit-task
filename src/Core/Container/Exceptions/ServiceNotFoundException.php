<?php

namespace Core\Container\Exceptions;
use Psr\Container\NotFoundExceptionInterface;

class ServiceNotFoundException extends \InvalidArgumentException implements NotFoundExceptionInterface
{
    public function __construct(string $msg)
    {
        parent::__construct($msg);
    }
}