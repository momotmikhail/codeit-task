<?php

namespace Core\Router\Exceptions;

use Core\Router\IRoute;
use LogicException;

class RouteAlreadyExistsException extends LogicException
{
    private $route;

    public function __construct(IRoute $route)
    {
        $this->route = $route;
        parent::__construct("IRoute ". $route->getRoute()." has already exists");
    }
}