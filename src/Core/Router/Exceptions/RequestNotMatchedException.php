<?php


namespace Core\Router\Exceptions;


use LogicException;
use Psr\Http\Message\ServerRequestInterface;

class RequestNotMatchedException extends LogicException
{
    private $request;

    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
        parent::__construct("Request not matched");
    }
}