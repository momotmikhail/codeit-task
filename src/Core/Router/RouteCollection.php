<?php

namespace Core\Router;

use Core\Router\Exceptions\RouteAlreadyExistsException;

class RouteCollection
{
    private $routes = [];

    public function get(string $route, $handler) : void
    {
        $this->addRoute(new Route($route, $handler, "GET"));
    }
    public function post(string $route, $handler) : void
    {
        $this->addRoute(new Route($route, $handler, "POST"));
    }
    public function put(string $route, $handler) : void
    {
        $this->addRoute(new Route($route, $handler, "PUT"));
    }
    public function delete(string $route, $handler) : void
    {
        $this->addRoute(new Route($route, $handler, "DELETE"));
    }
    public function any(string $route, $handler) : void
    {
        $this->addRoute(new Route($route, $handler, ''));
    }
    public function add(string $name, $handler, string $methods): void
    {
        $this->addRoute(new Route($name, $handler, $methods));
    }

    public function addRoute(Route $route): void
    {
        if ($this->routeExists($route))
            throw new RouteAlreadyExistsException($route);

        $this->routes[] = $route;
    }
    public function routeExists(Route $route) : bool
    {
        foreach ($this->routes as $elem){
            if ($elem->getRoute() == $route->getRoute() &&
                $elem->getMethod() == $route->getMethod())
                return true;
        }
        return false;
    }

    public function getRoutes() : array
    {
        return $this->routes;
    }
}