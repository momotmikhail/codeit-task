<?php

namespace Core\Router;

use Core\Interfaces\Router\IRoute;

class Route implements IRoute
{
    private $route;
    private $handler;
    private $method;

    public function __construct(string $route, $handler, string $method)
    {
        $this->route = $route;
        $this->handler = $handler;
        $this->method = $method;
    }

    public function getRoute() : string
    {
        return $this->route;
    }

    public function setRoute(string $route): void
    {
        $this->route = $route;
    }

    public function getHandler()
    {
        return $this->handler;
    }

    public function setHandler($handler): void
    {
        $this->handler = $handler;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    public function getRouteRegEx(): string
    {
        $route = preg_replace('/{([a-z]+):([^\}]+)}/', '(?P<\1>\2)', $this->route);
        $route = '#^' . $route . '$#';

        return $route;
    }
}