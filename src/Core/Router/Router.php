<?php

namespace Core\Router;

use Core\Interfaces\Router\IRoute;
use Core\Interfaces\Router\IRouter;
use Core\Router\Exceptions\RequestNotMatchedException;
use Psr\Http\Message\ServerRequestInterface;

class Router implements IRouter
{
    private $routeCollection;

    public function __construct(RouteCollection $collection)
    {
        $this->routeCollection = $collection;
    }

    public function match(ServerRequestInterface $request): Result
    {
        $url = ($request->getUri()->getPath() == "") ? "/" : $request->getUri()->getPath();
        $method = $request->getMethod();
        $routes = $this->routeCollection->getRoutes();

        foreach ($routes as $route) {
            if (preg_match($route->getRouteRegEx(), $url, $matches)) {
                if ($route->getMethod() == $method)
                    return new Result($route->getRoute(),
                                      $route->getHandler(),$this->makeResultAttributes($matches));
            }
        }
        throw new RequestNotMatchedException($request);
    }

    public function addRoute(IRoute $route)
    {
        $this->routeCollection->addRoute($route);
    }

    //delete useless fields from $matches array(from preg_match())
    private function makeResultAttributes(array $matches) : array
    {
        if (isset($matches[0])){

            $matches['URI'] = $matches[0];
            unset($matches[0]);

            for ($i = 1; $i<count($matches); $i++){
                if (isset($matches[$i])){
                    unset($matches[$i]);
                }
            }
        }
        return $matches;
    }

}