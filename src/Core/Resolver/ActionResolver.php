<?php

namespace Core\Resolver;

use Core\Interfaces\IResolver;
use Core\Pipeline\Pipeline;
use Core\Resolver\Exceptions\UndefinedHandlerTypeException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;

class ActionResolver implements IResolver
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function resolve($handler): callable
    {
        if (is_string($handler) && $this->container->has($handler))
        {
            return function (ServerRequestInterface $request, callable $next) use ($handler)
            {
                $object = $this->container->get($handler);
                return  $object($request, $next);
            };
        }

        if (is_array($handler)){
            return $this->createPipe($handler);
        }

        throw new UndefinedHandlerTypeException($handler);
    }

    private function createPipe(array $handlers){

        $pipeline = new Pipeline();
        foreach ($handlers as $handler){

            $pipeline->pipe($this->resolve($handler));
        }
        return $pipeline;
    }
}