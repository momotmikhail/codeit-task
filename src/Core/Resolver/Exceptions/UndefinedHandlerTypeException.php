<?php


namespace Core\Resolver\Exceptions;


use Throwable;

class UndefinedHandlerTypeException extends \LogicException
{
    private $handler;

    public function __construct($handler)
    {
        $this->handler = $handler;
        parent::__construct("Unresolved type of route handler");
    }

}