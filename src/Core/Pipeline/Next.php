<?php

namespace Core\Pipeline;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Next
{
    private $actions = [];
    private $default;

    public function __construct(array $actions, callable $default)
    {
        $this->actions = $actions;
        $this->default = $default;
    }

    public function __invoke(ServerRequestInterface $request) : ResponseInterface
    {
        $current = array_shift($this->actions);

        if ($current == null){
            return ($this->default)($request);
        }

        return $current($request, function (ServerRequestInterface $request){
            return $this($request);
        });
    }
}