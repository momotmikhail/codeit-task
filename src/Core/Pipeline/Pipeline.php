<?php

namespace Core\Pipeline;

use Core\Interfaces\Http\IAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Pipeline implements IAction
{
    private $actions = [];

    public function pipe($action) : void
    {
        $this->actions[] = $action;
    }

    public function __invoke(ServerRequestInterface $request, callable $default = null): ResponseInterface
    {
        $delegate = new Next($this->actions, $default);
        return $delegate($request);
    }
}