<?php


namespace App\Services;


use App\Entities\User;
use Core\Http\Session;
use Doctrine\ORM\EntityManagerInterface;

class AuthService
{
    private $entityManager;
    private $session;

    private $hiddenRoutes = [];
    private $protectedRoutes = [];

    public function __construct(EntityManagerInterface $entityManager, Session $session, array $hiddenRoutes, $protectedRoutes)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->hiddenRoutes = $hiddenRoutes;
        $this->protectedRoutes = $protectedRoutes;
    }

    public function login(string $login, string $password) : bool
    {
        $repository = $this->entityManager->getRepository(User::class);
        $user = $repository->findOneBy(["login"  => $login]);

        if ($user){
            if ($user->getLogin() == $login && $user->getPassword() == $password){
                $this->session->start();
                $this->session->set("user", $user);

                return true;
            }
        }

        return false;
    }

    public function logout()
    {
        $this->session->destroy();
    }

    public function isAuth(): bool
    {
        if ($this->session->cookieExists())
            $this->session->start();

        if ($this->session->sessionExists() && $this->session->get("user")!= null)
            return true;
        return false;
    }

    public function getCurrentUser(): ?User
    {
        if ($this->isAuth())
            return $this->session->get("user");
        return null;
    }

    public function isProtectedRoute(string $route): bool
    {
        foreach ($this->protectedRoutes as $protectedRoute){
            $protectedRoute = preg_replace('/{([a-z]+):([^\}]+)}/', '(?P<\1>\2)', $protectedRoute);
            $protectedRoute = '#^' . $protectedRoute . '$#';
            if (preg_match($protectedRoute, $route, $matches))
                return true;
        }
        return false;
    }

    public function isHiddenRoute(string $route): bool
    {
        foreach ($this->hiddenRoutes as $hiddenRoute){
            $hiddenRoute = preg_replace('/{([a-z]+):([^\}]+)}/', '(?P<\1>\2)', $hiddenRoute);
            $hiddenRoute = '#^' . $hiddenRoute . '$#';
            if (preg_match($hiddenRoute, $route, $matches))
                return true;
        }
        return false;
    }

}