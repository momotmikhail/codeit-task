<?php

namespace App\Services;

use App\Entities\Page;
use App\Entities\User;
use Doctrine\ORM\EntityManagerInterface;

class PageService
{
    private $entityManager;
    private $coreRoutes = [
        "login",
        "admin",
        "admin/page/index",
        "admin/page/create"
    ];

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAll()
    {
        $repository = $this->entityManager->getRepository(Page::class);
        return $repository->findAll();
    }

    public function getByUrl(string $url) : ?Page
    {
        /**
         * @var Page $page
         */
        $repository = $this->entityManager->getRepository(Page::class);
        $page = $repository->findOneBy(["url" => $url]);
        return $page;
    }

    public function create(string $title, string $url, string $description, string $content,  User $user)
    {
        $title = trim(htmlspecialchars($title));
        $description = trim(htmlspecialchars($description));
        $content = trim($content);
        $url = trim(htmlspecialchars($url));

        $page = new Page();
        $page->setTitle($title);
        $page->setDescription($description);
        $page->setContent($content);
        $page->setUrl("/" . $url);
        $page->setUser($user);

        $this->entityManager->merge($page);
        $this->entityManager->flush();

    }

    public function remove(int $id)
    {
        $repository = $this->entityManager->getRepository(Page::class);
        $page = $repository->find($id);

        if ($page != null){
            $this->entityManager->remove($page);
            $this->entityManager->flush();
        }

    }

    public function pageValidation($title, $url, $description, $content) : array
    {
        $errors = [];
        if ($error = $this->titleValidation($title))
            $errors[] = $error;
        if ($error = $this->URLValidation($url))
            $errors[] = $error;
        if ($error = $this->descriptionValidation($description))
            $errors[] = $error;
        if ($error = $this->contentValidation($content))
            $errors[] = $error;
        return $errors;
    }

    private function titleValidation(string $title): ?string
    {
        if (empty($title))
            return "Please fill title field";
        if (strlen($title) > 255)
            return "Title field is so long";
        return null;
    }

    private function URLValidation(string $url): ?string
    {
        if (empty($url))
            return "Please fill URL field";
        if (strlen($url) > 255)
            return "Title url is so long";
        if (!preg_match("/^[a-z0-9]+(?:-[a-z0-9]+)*$/", $url))
            return "Incorrect URL. Please use only english letters in lower case, numbers and \"-\" symbol";
        if ($this->isCoreRoute($url))
            return "This route is application related";
        if ($this->getByUrl("/" . $url))
            return "This route is already exists";
        return null;
    }

    private function descriptionValidation(string $description): ?string
    {
        if (empty($description))
            return "Please fill description field";
        if (strlen($description) > 255)
            return "Title description is so long";
        return null;
    }

    private function contentValidation(string $content): ?string
    {
        if (empty($content))
            return "Please fill content field";
        return null;
    }

    private function isCoreRoute(string $route) : bool
    {
        if (in_array($route ,$this->coreRoutes)){
            return $route;
        }
        return false;
    }
}