<?php

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="users",
 *    uniqueConstraints={
 *        @ORM\UniqueConstraint(name="users_unique", columns={"login"})
 *    }
 * )
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $login;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="user")
     */
    private $pages;

    public function __construct()
    {
        $pages = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function setLogin($login): void
    {
        $this->login = $login;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }



}