<?php


namespace App\Middlewares;


use App\Controllers\Admin\Page\CreateFormAction;
use App\Services\PageService;
use Core\Interfaces\Http\IAction;
use Core\Interfaces\IView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ValidateCreatePageRequest implements IAction
{
    private $pageService;
    private $view;

    public function __construct(PageService $pageService, IView $view)
    {
        $this->pageService = $pageService;
        $this->view = $view;
    }

    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {

        $errors =  $this->pageService->pageValidation(
            $request->getParsedBody()["title"],
            $request->getParsedBody()["url"],
            $request->getParsedBody()["description"],
            $request->getParsedBody()["content"]
        );

        if (empty($errors)){
            return $next($request);
        }
        return (new CreateFormAction($this->pageService, $this->view))(
            $request->withAttribute("errors", $errors)
                    ->withAttribute("oldForm", $request->getParsedBody())
        );

    }
}