<?php


namespace App\Middlewares;


use App\Services\AuthService;
use Core\Interfaces\Http\IAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

class AuthMiddleware implements IAction
{
    private $authService;

    private $redirectHiddenTo = "/admin";
    private $redirectProtectedTo = "/login";

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {
        $route = $request->getUri()->getPath();

        if ($this->authService->isAuth()){
            if ($this->authService->isHiddenRoute($route))
                return new RedirectResponse($this->redirectHiddenTo);
        }
        else{
            if ($this->authService->isProtectedRoute($route))
                return new RedirectResponse($this->redirectProtectedTo);
        }

        return $next($request->withAttribute("user", $this->authService->getCurrentUser()));
    }
}