<?php


namespace App\Controllers\Admin\Page;


use App\Services\PageService;
use Core\Interfaces\Http\IAction;
use Core\Interfaces\IView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;

class CreateAction implements IAction
{
    private $pageService;
    private $view;

    public function __construct(PageService $pageService, IView $view)
    {
        $this->pageService = $pageService;
        $this->view = $view;
    }

    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {
        $this->pageService->create(
            $request->getParsedBody()["title"],
            $request->getParsedBody()["url"],
            $request->getParsedBody()["description"],
            $request->getParsedBody()["content"],
            $request->getAttribute("user")
        );

        return new RedirectResponse("/admin/page/index");
    }
}