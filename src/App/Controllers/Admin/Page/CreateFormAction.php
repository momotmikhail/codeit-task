<?php


namespace App\Controllers\Admin\Page;


use App\Services\PageService;
use Core\Interfaces\Http\IAction;
use Core\Interfaces\IView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

class CreateFormAction implements IAction
{

    private $pageService;
    private $view;

    public function __construct(PageService $pageService, IView $view)
    {
        $this->pageService = $pageService;
        $this->view = $view;
    }

    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {
        return new HtmlResponse($this->view->render("app/admin/page/create",[
            "user" =>  $request->getAttribute("user"),
            "errors" => $request->getAttribute("errors"),
            "oldForm" => $request->getAttribute("oldForm"),
        ]));
    }
}