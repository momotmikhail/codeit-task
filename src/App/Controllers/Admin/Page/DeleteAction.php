<?php


namespace App\Controllers\Admin\Page;


use App\Services\PageService;
use Core\Interfaces\Http\IAction;
use Core\Interfaces\IView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

class DeleteAction implements IAction
{
    private $pageService;

    public function __construct(PageService $pageService)
    {
        $this->pageService = $pageService;
    }
    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {
        $this->pageService->remove($request->getAttribute("id"));
        return new RedirectResponse("/admin/page/index");
    }
}