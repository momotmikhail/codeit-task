<?php


namespace App\Controllers\Admin;


use Core\Interfaces\Http\IAction;
use Core\Interfaces\IView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

class CabinetAction implements IAction
{
    private $view;

    public function __construct(IView $view)
    {
        $this->view = $view;
    }

    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {
        return new HtmlResponse($this->view->render("app/admin/cabinet",[
            "user" =>  $request->getAttribute("user")
        ]));
    }
}