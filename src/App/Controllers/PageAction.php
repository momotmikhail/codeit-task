<?php

namespace App\Controllers;


use App\Services\PageService;
use Core\Interfaces\Http\IAction;
use Core\Interfaces\IView;
use Core\Router\Exceptions\RequestNotMatchedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

class PageAction implements IAction
{
    private $pageService;
    private $view;

    public function __construct(PageService $pageService, IView $view)
    {
        $this->pageService = $pageService;
        $this->view = $view;
    }

    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {
        if ($page = $this->pageService->getByUrl($request->getUri()->getPath()))
            return new HtmlResponse($this->view->render("app/page", [
                "page" => $page,
                "user" => $request->getAttribute("user")]));
        throw new RequestNotMatchedException($request);
    }
}