<?php


namespace App\Controllers\Errors;


use Core\Interfaces\Http\IAction;
use Core\Interfaces\IView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

class PageNotFound implements IAction
{

    private $view;

    public function __construct(IView $view)
    {
        $this->view = $view;
    }

    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {
        return new HtmlResponse($this->view->render("errors/404"));
    }
}