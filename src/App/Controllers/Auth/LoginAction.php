<?php

namespace App\Controllers\Auth;

use App\Services\AuthService;
use Core\Interfaces\Http\IAction;
use Core\Interfaces\IView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

class LoginAction implements IAction
{
    private $service;
    private $view;

    public function __construct(AuthService $authService, IView $view)
    {
        $this->service = $authService;
        $this->view = $view;
    }

    public function __invoke(ServerRequestInterface $request, callable $next= null): ResponseInterface
    {
        $login = $request->getParsedBody()["login"];
        $password = $request->getParsedBody()["password"];
        $loginResult = $this->service->login($login, $password);


        if ($loginResult)
            return new RedirectResponse("/admin");
        else
            return (new LoginShowAction($this->view))($request->withAttribute("error", "Incorrect login or password"));
    }
}