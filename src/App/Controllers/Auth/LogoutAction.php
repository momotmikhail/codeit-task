<?php


namespace App\Controllers\Auth;


use App\Services\AuthService;
use Core\Interfaces\Http\IAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

class LogoutAction implements IAction
{
    private $service;

    public function __construct(AuthService $authService)
    {
        $this->service = $authService;
    }

    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {
        $this->service->logout();
        return new RedirectResponse("/");
    }
}