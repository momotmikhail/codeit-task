<?php


namespace App\Controllers;


use App\Services\PageService;
use Core\Interfaces\Http\IAction;
use Core\Interfaces\IView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

class HomeAction implements IAction
{
    private $view;
    private $pageService;

    public function __construct(IView $view, PageService $pageService)
    {
        $this->view = $view;
        $this->pageService = $pageService;
    }

    public function __invoke(ServerRequestInterface $request, callable $next = null): ResponseInterface
    {
        $pages = $this->pageService->getAll();
        return new HtmlResponse($this->view->render("app/home",[
            "user" => $request->getAttribute("user"),
            "pages" => $pages
        ]));
    }
}